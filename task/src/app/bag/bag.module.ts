import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BagRoutingModule } from './bag-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BagRoutingModule
  ]
})
export class BagModule { }
