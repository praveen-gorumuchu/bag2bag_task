import { Pipe, PipeTransform } from '@angular/core';
import { retry } from 'rxjs';
import { Activity } from '../models/activity.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: Activity[], inputValue: string,): Activity[] {
    if(inputValue && inputValue.length === 0) return [];
    if(inputValue === '') return value;
      const valid = inputValue && inputValue.length >= 2 && value && value.length > 0;
      inputValue = valid ? inputValue.toLocaleLowerCase() : '';

       return value.filter(item => {
         return this.activityContainsInput(item, inputValue);
      
      })
    }

   private activityContainsInput(value: Activity, inputName: string) : boolean{
      inputName = inputName.toLocaleLowerCase();
    const filterTerms = inputName.split(' ');
    for (const filterTerm of filterTerms) {
      const hasFilterTerm = this.includesFilterInput(value, filterTerm);
      if (!hasFilterTerm) return false;
    }

    return true
    }

    includesFilterInput(value: Activity, inputValue: string){
      return value.name?.toLocaleLowerCase().includes(inputValue)
    }



}
