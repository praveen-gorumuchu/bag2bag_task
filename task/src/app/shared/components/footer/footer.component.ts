import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  home: boolean = false;
  store: boolean = false;
  profile: boolean = false;
  alert: boolean = false;
  btnName: string = '';

  constructor(router: Router) { }

  ngOnInit(): void {}

  onBtnClick(btnName: string){
    this.btnName = btnName;
  }

}
