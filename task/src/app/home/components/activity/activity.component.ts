import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Activity } from 'src/app/shared/models/activity.model';


@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

  activityPath = '../../../../assets/json/activites.json';
  activityArray:any = [];
  activityName:string = '';

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.httpClient.get(this.activityPath).subscribe(data =>{
      console.log(data);
      this.activityArray = data;
  });
}

onActivity(data:any){
  window.open(data.link, '_blank')
}

}
