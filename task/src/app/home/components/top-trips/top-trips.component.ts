import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-trips',
  templateUrl: './top-trips.component.html',
  styleUrls: ['./top-trips.component.scss']
})
export class TopTripsComponent implements OnInit {
  tripsPath = '../../../../assets/json/trips.json';
  tripsArray: any = [];

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.httpClient.get(this.tripsPath).subscribe(data =>{
      console.log(data);
      this.tripsArray = data;
  });
  }

}
