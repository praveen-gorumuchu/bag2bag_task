import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopTripsComponent } from './top-trips.component';

describe('TopTripsComponent', () => {
  let component: TopTripsComponent;
  let fixture: ComponentFixture<TopTripsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopTripsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTripsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
